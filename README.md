# docker-alpine-tester [![](https://images.microbadger.com/badges/image/jorgeandrada/docker-alpine-tester.svg)](https://microbadger.com/images/jorgeandrada/docker-alpine-tester "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/jorgeandrada/docker-alpine-tester.svg)](https://microbadger.com/images/jorgeandrada/docker-alpine-tester "Get your own version badge on microbadger.com")
Docker Alpine Tester: para realizar pruebas de red.

Programas preinstalados:
-   drill
-   htop
-   bind-tools
-   wget
-   curl
-   nmap
-   mariadb-client
-   vim
